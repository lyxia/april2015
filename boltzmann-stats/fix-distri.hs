import Data.Functor
import Data.List
import System.Environment

{- In this case (depth), since a coefficient only depends on the previous ones,
   the prefix of length n of the result is actually fully and exactly determined
   after n iterations.
-}
refine :: Num a => a -> [a] -> [a]
refine a ps
  = (1-a) : [ a * (2 * q * s + q * q) | (q, s) <- zip ps (scanl (+) 0 ps) ]

-- ./prog 0.4 10 # iterates refine 10 times with the parameter 0.4
main = do
  [a, n] <- getArgs
  let p0 = [1]
  print $ iterate (refine (read a :: Double)) p0 !! (read n :: Int)

