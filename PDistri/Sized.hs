module PDistri.Sized where

import Control.Monad
import Data.List
import Data.Maybe

-- @bySizeAndDepth !! n !! d@: Number of trees of size n and depth d
bySizeAndDepth :: (a -> a -> a) -> (a -> a -> a) -> a -> a -> [[a]]
bySizeAndDepth (*) (+) one zero = memo
  where
    memo = row `fmap` [0 ..]
    row 0 = [one] -- Size 0, depth 0
    row i
      = let prev = take i memo
            zipped = zipWith f prev (reverse prev)
        in zero : foldl1' (zipWith' (+)) zipped
    f sized1 sized2
      = let fFst = zipWith
                    (\x ys -> foldl1' (+) . map (x *) $ ys)
                    sized1
                    (tail (inits sized2) ++ repeat sized2)
            fSnd = zipWith
                    (\xs y -> foldl' (+) zero . map (* y) $ xs)
                    (inits sized1 ++ repeat sized1)
                    sized2
        in zipWith' (+) fFst fSnd
    zipWith' (+) [] bs = bs
    zipWith' (+) as [] = as
    zipWith' (+) (a : as) (b : bs)
      = a + b : zipWith' (+) as bs

countBySD :: [[Integer]]
countBySD = bySizeAndDepth (*) (+) 1 0

