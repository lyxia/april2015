module Gen.Boltzmann where

import Common.Tree
import Control.Monad
import Data.Function
import Data.Functor
import Test.QuickCheck

gen :: Double -> Gen Tree
gen a
  = fix $ \g -> weightedPick a (liftM2 Node g g) (return Leaf)

weightedPick :: Double -> Gen a -> Gen a -> Gen a
weightedPick p g h = do
  x <- choose (0, 1)
  if x < p then g else h

incr n [] = replicate n 0 ++ [1]
incr 0 (x : xs) = (x + 1) : xs
incr n (x : xs) = x : incr (n - 1) xs

