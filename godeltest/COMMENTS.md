The results of the paper were mostly reproduced.

- The results of the naive (quickcheck) generator were worse than reported
in the paper. High depths (>= 8) are improbable.

- No comment on the geometric distribution based generator.

- The differential evolution implemented here is far from robust;
several retries are often necessary to obtain graphs similar to the paper's.

  But much better performance has also been encountered for DecayHistogram:
`best0.txt`, `best6.txt`.

  In all cases, the actual parameters are situated on the edge of
the parameter space, and the simulated distributions of the choice point
exhibit more than two regimes at different depth intervals,
which seems like an artifact not entirely within the original intent of the
DecayHistogram model.

  For example: the typical tree generated with parameters from `best6.txt`
has 3 linear branches of length around 36. 
