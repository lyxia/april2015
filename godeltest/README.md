godeltest
=========

Dependencies
------------

    quickcheck
    transformers
    random

Compilation
-----------

    ghc godeltest

Usage
-----

    ./godeltest (arb|hist|geom)

will: in the case `(hist|geom)`, learn parameters of a random tree generator
to target size 100 and depth `(36|6)`, using a genetic algorithm;
generate 10000 trees;
and plot the histogram of sizes (x-axis, 0 to 200) and depths (y-axis, 0 to 58).

A dot indicates no trees, and a hex digit indicates
the relative non-zero density of trees in that rectangle.

