{- The tree datastructure for reference. -}
module Common.Tree where

import Data.Function

data Tree
  = Leaf {- with weight (1-a) (or b...?) -}
  | Node Tree Tree {- with weight a -}
  deriving (Eq, Show)

size :: Num a => Tree -> a
size Leaf = 0
size (Node l r) = 1 + size l + size r

depth :: (Num a, Ord a) => Tree -> a
depth Leaf = 0
depth (Node l r) = 1 + (max `on` depth) l r

toString :: Tree -> String
toString Leaf = "0"
toString (Node l r) = '1' : toString l ++ toString r

fromString :: String -> Tree
fromString xs =
  let (t, tl) = fromStringAux xs
  in if null tl then t else parseError
  where
    fromStringAux :: String -> (Tree, String)
    fromStringAux ('0' : xs) = (Leaf, xs)
    fromStringAux ('1' : xs) =
      let (l, rxs) = fromStringAux xs
          (r, tl) = fromStringAux rxs
      in (Node l r, tl)
    fromStringAux [] = parseError
    parseError = error $ "Parse error: " ++ show xs

