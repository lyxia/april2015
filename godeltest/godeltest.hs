-- quickcheck transformers random
--
-- A reproduction of GodelTest results, from the paper
-- Finding Test Data with Specific Properties via Metaheuristic Search,
-- Feldt et al..
{-# LANGUAGE ScopedTypeVariables, NamedFieldPuns, RecordWildCards #-}
module Main where

import Control.Applicative
import Data.Char (intToDigit)
import Data.Function
import Data.List
import Data.Maybe
import System.IO
import System.Environment
import Text.Printf

import Control.Monad
import Control.Monad.Trans.Class
import Control.Monad.Trans.Maybe
import Control.Monad.Trans.State

import System.Random

import Test.QuickCheck

-------------------
-- Type of trees to generate
data Tree = Node [Tree]
  deriving (Eq, Show, Ord)

depth (Node []) = 0
depth (Node xs) = 1 + maximum (depth <$> xs)

size (Node xs) = 1 + sum (size <$> xs)

-- The naive (QuickCheck) Tree generator from the paper
instance Arbitrary Tree where
  arbitrary = sized tree'
    where
      tree' 0 = return $ Node []
      tree' n = Node <$> resize (n-1) (listOf' arbitrary)

listOf' gen = sized $ \n ->
  do
    k <- choose (0, n)
    if k == 0
    then return []
    else vectorOf k (resize ((n+k-1) `div` k) gen)

-------------------
-- A generator that should have a behavior close to the GodelTest
-- example in the paper

-- Max number of calls, abort if reached.
stop = 1000

-- Each "choice point" becomes a generator parameter,
-- which can examine some runtime information,
-- in this case the recursion depth.
--
-- The recursion depth is passed around as a function argument,
-- the total number of calls is recorded using StateT, and failure
-- is enabled by MaybeT.
treeWithD :: (Int -> Gen Int) -> Gen (Maybe Tree)
treeWithD gen = flip evalStateT 0 . runMaybeT $ treeWithD' 0
  where
    treeWithD' :: Int -> MaybeT (StateT Int Gen) Tree
    treeWithD' d = do
      incr
      k <- lift . lift $ gen d
      Node <$> replicateM k (treeWithD' (d+1))
    incr = do
      c <- lift get
      guard (c < stop)
      lift $ put (c+1)

-- Geometric distribution with parameter (1-p) (p: probability of failure)
geom :: Double -> Gen Int
geom p = do
    x <- choose (0.0, 1.0)
    if x == 0.0
    then geom p
    else return $ floor (log x / log p)

-- "Histogram", or explicit, distribution.
-- A list [p0 .. pn] describes the distribution
-- of a random variable X with values in [0 .. n] and probability
-- P(X = i) proportional to pi.
hist :: [Double] -> Gen Int
{-# INLINE hist #-}
hist ps = do
    k <- choose (0.0, sum ps)
    return $ select 0 k ps
  where
    select n k (p : ps)
      | k < p = n
      | otherwise = select (n+1) (k-p) ps
    select n _ [] = n

-- "Decay" transformer for the histogram distribution.
-- decayHistP d [r] [p] = p * (r ^ p)
-- (p, r in the real interval [0 ; 1])
-- This and the scan on rs are so that: as d -> infinity,
-- the generator should be more likely to return 0
-- (which often indicates termination of some kind).
decayHistP :: Int -> [Double] -> [Double] -> [Double]
{-# INLINE decayHistP #-}
decayHistP d = zipWith ((*) . (^ d)) . scanl1 (*)

{-# INLINE decayHist #-}
decayHist rs ps d = hist $ decayHistP d rs ps

treeDecayHist rs ps | length rs == length ps && all (<= 1) rs
  = treeWithD (decayHist rs ps)
  | otherwise = error $ show rs ++ " " ++ show ps

-- "Decay" transformer for the geometric distribution.
decayGeom r p d = geom (p * r ^ d)

treeDecayGeom r p | p < 1 && r <= 1 = treeWithD (decayGeom r p)
  | otherwise = error $ show r ++ " " ++ show p

-- Normalize a vector
normal :: [Double] -> [Double]
normal xs = (/ sum xs) <$> xs

-- The actual distributions resulting from the decay transformer,
-- at each depth.
paramByDepth rs ps =
  [ normal $ decayHistP d rs ps | d <- [0 ..] ]

data ParamDE = ParamDE
  { pop :: Int {- Population size -}
  , w :: Double {- Weight -}
  , co :: Double {- Crossover proba -}
  , g :: Int {- Generations -}
  , s :: Int {- Fitness evaluations per individual and per generation -}
  }

-------------------
-- Differential evolution in R^n
-- A simpler version than what the authors of the paper did,
-- pretty much as described on wikipedia.
-- Multiple tries may be necessary to obtain results similar to the author's.
evolve
  n {- Vector length -}
  (f :: [Double] -> IO Double) {- Fitness -}
  (ParamDE {..})
  = do
    pool <- initDE n pop
    pool' <- forM pool $ \x -> (,) x <$> fit' x
    pool_ <- iterAndPrint g (join $ mapM . update) pool'
    return . fst $ minimumBy (compare `on` snd) pool_
  where
    fit' x = avg <$> replicateM s (f x)
    update pool (x, fx) = do
      [(a, _), (b, _), (c, _)] <- replicateM 3 . generate $ elements pool
      i <- randomRIO (0, n-1)
      y <- mutate i x a b c
      fy <- fit' y
      return $ if fy < fx then (y, fy) else (x, fx)
    mutate n (x : xs) (a : as) (b : bs) (c : cs) = do
      ys <- mutate (n-1) xs as bs cs
      r <- randomRIO (0.0, 1.0)
      return $
        (if n == 0 || r < co then a + w * (b - c) else x) : ys
    mutate _ _ _ _ _ = return []

avg ls = sum ls / fromIntegral (length ls)

-- Initialize with random vectors.
initDE :: Int -> Int -> IO [[Double]]
initDE n pop = replicateM pop (replicateM n $ randomRIO (-2, 2))

-- Map R^n -> (0;1)^n
rescale :: [Double] -> [Double]
rescale = map ((/ 2.0) . (+ 1.0) . tanh)

-- Fitness function for geometric distributions
geomFitness targetDepth targetSize v = do
    let [r, p] = rescale v
    unwrap targetDepth targetSize $
      treeDecayGeom r p

-- Fitness function for histogram distributions
histFitness targetDepth targetSize v = do
    let (rs, ps) = byTwo $ rescale v
    unwrap targetDepth targetSize $
      treeDecayHist rs ps

unwrap td ts gt =
  f <$> generate gt
  where
    f = (fromIntegral :: Int -> Double) . maybe
      (stop ^ (2 :: Int))
      (\t -> (td - depth t) ^ (2 :: Int) + (ts - size t) ^ (2 :: Int))

byTwo [] = ([], [])
byTwo (r : p : xs) = let (rs, ps) = byTwo xs in (r : rs, p : ps)

-- Find parameters for the geometric distribution
-- targeting a size 100 and a depth 6.
geomEvolve pde@(ParamDE {..})
  = do
    v <- evolve 2 fit pde
    let [r, p] = rescale v
    f <- avg <$> replicateM s (fit v)
    return (r, p, f)
  where
    fit = geomFitness targetDepth targetSize
    targetDepth = 6
    targetSize = 100

geomPDE = ParamDE
  { pop = 40
  , g = 20
  , s = 10
  , w = 0.2
  , co = 0.3
  }

-- Find parameters of a histogram distribution
-- targeting a size 100 and depth 36.
histEvolve pde@(ParamDE {..})
  = do
    v <- evolve 10 fit pde
    let (rs, ps) = byTwo $ rescale v
    f <- avg <$> replicateM s (fit v)
    return (rs, ps, f)
  where
    fit = histFitness targetDepth targetSize
    targetDepth = 36
    targetSize = 100

histPDE = ParamDE
  { pop = 20
  , g = 20
  , s = 10
  , w = 0.7
  , co = 0.5
  }

-------------------
-- Printing 2D histograms and more stats in the command line
data Histo = Histo
  { histo :: [[Int]],
    dStep :: Int, -- rows width (row r contains [r * dStep, (r+1) * dStep])
    sStep :: Int } -- cols width

data MoreStats = MStats
  { total :: Int,
    hits :: Int,
    miss :: Int,
    avgDepth :: Double,
    avgSize :: Double }
  deriving (Show)

data Stats = Stats MoreStats Histo

histogram :: Int -> Int -> [Tree] -> Histo
histogram dStep sStep out
  = foldl' update ini out
  where
    ini = Histo
      { histo = repeat (repeat 0),
        dStep,
        sStep }
    update stats@(Histo {..}) t =
      let { i = depth t ; j = size t }
      in stats { histo = incr (i `div` dStep) (j `div` sStep) histo }
    incr i j hist = modifyL i (modifyL j (+1)) hist

modifyL 0 f (x : xs) = let y = f x in y `seq` (y : xs)
modifyL n f (x : xs) = let ys = modifyL (n-1 :: Int) f xs in ys `seq` (x : ys)

stats :: Int -> Int -> [Maybe Tree] -> Stats
stats dStep sStep out = Stats s h
  where
    s = MStats
      { total = length out,
        hits = length o,
        miss = total s - hits s,
        avgDepth = avg (depth <$> o),
        avgSize = avg (size <$> o) }
    h = histogram dStep sStep o
    o = catMaybes out

plotHistogram :: Int -> Int -> Histo -> IO ()
plotHistogram rows cols (Histo {..}) = do
    forM_ hStr putStrLn
  where
    h' = take cols <$> take rows histo
    z = sum $ concat h'
    count = rows * cols
    hStr = reverse $
      zipWith (\r d -> map f r ++ " " ++ show d) h' [0, dStep..]
    f 0 = '.'
    f x = intToDigit . min 15 . max 0 $ (10 * x * rows * cols) `div` z

main = do
    a : as <- getArgs
    case a of
      "help" -> do
        self <- getProgName
        mapM_ putStrLn
          [ self ++ " (arb|hist|geom)"
          , "arb   the naive (QuickCheck) tree generator"
          , "hist  the \"histogram distribution\" based generator"
          , "geom  the geometric distribution based generator"
          ]
      "test" -> do
        t <- generate $ resize 100 arbitrary
        print t
        putStrLn $ "depth:" ++ show (depth t) ++ " size:" ++ show (size t)
      "arb" -> do
        stats' $ Just <$> (resize 100 arbitrary)
      "hist" -> do
        let pde = argParam histPDE as
        -- If the fitness level is not good enough, retry
        pa@(rs, ps, f) <- while (\(_, _, f) -> f < 2000) $ histEvolve pde
        print pa
        mapM_
          ((>> putStrLn "") . mapM_ ((>> putStr " ") . printD))
          (sparse 3 . take 40 $ paramByDepth rs ps)
        stats' (treeDecayHist rs ps)
      "geom" -> do
        let pde = argParam pde as
        pa@(r, p, f) <- geomEvolve pde
        print pa
        stats' (treeDecayGeom r p)
      _ -> putStrLn a
  where
    stats' gen = do
      ts <- generate $ replicateM 10000 gen
      let Stats s h = stats 2 5 ts
      print s
      plotHistogram 30 40 h
    -- Set some parameters explicitly
    argParam pde as =
      case as of
        [] -> pde
        name : x : as ->
          let pde' = case name of
                "pop" -> pde { pop = read x }
                "w" -> pde { w = read x }
                "co" -> pde { co = read x }
                "g" -> pde { g = read x }
                "s" -> pde { s = read x }
                _ -> error $ "Unknown parameter name: " ++ name
          in argParam pde' as
        _ -> error "Invalid arguments"

-- Some helper functions
printD :: Double -> IO ()
printD = printf "%.2f"

sparse n = sparse' 0
  where
    sparse' _ [] = []
    sparse' 0 (x : xs) = x : sparse' n xs
    sparse' m (_ : xs) = sparse' (m-1) xs

-- Repeat monadic actions
while :: Monad m => (a -> Bool) -> m a -> m a
while p f = fix $ \redo -> do
  x <- f
  if p x then f else redo

-- Iterate a Kleisli arrow
iter :: Monad m => Int -> (a -> m a) -> a -> m a
iter 0 f x = return x
iter n f x = do
  y <- f x
  iter (n-1) f y

-- same as iter, but also updates a counter in stderr
--
-- Uses '\r' to go back to the beginning of the line.
-- May do odd things out of Linux.
iterAndPrint :: Int -> (a -> IO a) -> a -> IO a
iterAndPrint 0 f x = putStrLn "" >> return x
iterAndPrint n f x = do
    y <- f x
    let n' = show (n-1)
    hPutStr stderr $ "\r" ++ replicate (width-length n') ' ' ++ n'
    hFlush stderr
    iterAndPrint (n-1) f y
  where width = 3

