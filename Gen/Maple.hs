{-# LANGUAGE TemplateHaskell #-}
module Gen.Maple where

import Common.Tree

import Control.Applicative
import Control.Arrow
import Control.Monad
import Control.Lens
import Control.Lens.Lens
import Data.List
import Test.QuickCheck

data TreeAcc
  = AccLeaf 
  | AccUndef
  | AccNode { _l :: TreeAcc
            , _r :: TreeAcc }
  | TreeAcc { _inTree :: TreeAcc }
  deriving (Eq, Show)
makeLenses ''TreeAcc

treeAccToTree :: TreeAcc -> Tree
treeAccToTree AccLeaf = Leaf
treeAccToTree (AccNode l r) = Node (treeAccToTree l) (treeAccToTree r)
treeAccToTree (TreeAcc t) = treeAccToTree t
treeAccToTree t = error $ "not a complete TreeAcc" ++ show t

-- Removed redundancies
gen :: Double -> Gen Tree
gen mult = sized $ \size ->
    let t = TreeAcc { _inTree = AccUndef } in
    treeAccToTree <$> gen' mult size t [(1,inTree)]

gen' mult 0 t lenses
  = return $ foldl' (\t (_,l) -> set l AccLeaf t) t lenses
gen' mult n t lenses
  = do
    ((freq, lens), lenses') <- freqByRemove fst lenses
    node <- AccNode <$> pure AccUndef <*> pure AccUndef
    let t' = set lens node t
        f' = freq * mult
    gen' mult (n-1) t' $ (f', lens.l) : (f', lens.r) : lenses'

freqByRemove :: (a -> Double) -> [a] -> Gen (a, [a])
freqByRemove freqOf xs = do
    pick <$> choose (0, tot) <*> pure xs
  where
    tot = sum $ map freqOf xs
    pick p (x : xs)
      | p' <= 0 = (x, xs)
      | otherwise = second (x :) $ pick p' xs
      where p' = p - freqOf x

