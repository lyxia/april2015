-- @r !! n@ : Number of red rooted red-black trees with black-depth n
r = map (^ 2) b
-- @b !! n@ : Number of black rooted red-black trees with black-depth n
-- (not counting the leaves in the depth)
b = 1 : [ (zipWith (+) r b !! n) ^ 2 | n <- [0 ..] ]

-- Combinatorial explosion (doubly exponential)
main = do
  print $ take 5 r
  print $ take 5 b
