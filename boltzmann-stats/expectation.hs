{- Distribution of depths of binary trees
   from a Boltzmann sampler with parameter a -}

import Control.Monad
import Data.Functor
import System.Environment

byDepth :: Num a => a -> [a]
byDepth a = ps_d
  where
    ps_d = (1-a) : ps_dPlus1
    ps_dPlus1
      = zipWith
          (\p_d sum_0_to_dMinus1 ->
            a * (2 * p_d * sum_0_to_dMinus1 + p_d * p_d))
          ps_d
          (scanl (+) 0 ps_d)

expectedDepth :: Num a => Integer -> [a] -> a
expectedDepth n ps = sum [fromInteger d * p | (d, p) <- zip [0 .. n] ps]

main = do
    x <- read <$> getArg1
    let ps = byDepth (x :: Double)
    forM_ (zip [0 .. n] ps) print
    putStrLn
      $ "Expected depth (from first " ++ show n ++ " terms): "
        ++ show (expectedDepth n ps)
  where
    n = 10
    getArg1 = head <$> getArgs

