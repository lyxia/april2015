module Main where

import Common.Tree
import PDistri.Sized
import qualified Gen.Boltzmann as B
import qualified Gen.Maple as M
import qualified Gen.Sized as S
import Stats.Histo2D

import Control.Arrow
import Control.Monad

import Data.List

import Options.Applicative

import System.Environment
import Text.Printf

import Test.QuickCheck

main = join . execParser $ info (helper <*> parser) desc
  where desc = fullDesc

parser :: Parser (IO ())
parser = subparser
  $ command "compare" compareParser
  <> command "generate" generateParser
  <> command "histo" histoParser

compareParser :: ParserInfo (IO ())
compareParser = info parser desc
  where
    desc = briefDesc
      <> progDesc "Measure the similarity between the distribution of the \
        \Maple generator with the uniform distribution for various values of \
        \the weight parameter."
      <> footer "Output in the format \"(PARAM, SIMIL)\". \
        \Actually, only the projected distributions of depths are compared."
    parser = exec
      <$> option auto
        ( long "range" <> short 'r'
        <> value (1.5, 0.1, 2.2) <> showDefault
        <> metavar "(START,STEP,END)" )
      <*> option auto
        ( long "size" <> short 's'
        <> value 5 <> showDefault
        <> metavar "SIZE" )
      <*> option auto
        ( long "sample-n" <> short 'n'
        <> value 100 <> showDefault
        <> metavar "SAMPLE_N" )
    exec (lo, step, hi) s n = do
      let xs = zip [0 ..] . map fromInteger $ countBySD !! s
      forM_ [lo, lo+step .. hi] $ \x -> do
        samples <- replicateM n . fmap depth . generate . resize s $ M.gen x
        let ys = sort . map (second intToDouble) . group' $ samples
            v = normIp2 xs ys
        putStrLn $ printf "(%.2f, %.3f)" x v

data GenType
  = Boltz Double
  | Maple Int Double
  | Uniform Int

generateParser :: ParserInfo (IO ())
generateParser = info (helper <*> parser) desc
  where
    desc = briefDesc
      <> progDesc "Generate random trees."
      <> footer "If a -n SAMPLE_N parameter is specified, \
      \generate SAMPLE_N trees and print their depths. \
      \Otherwise, an encoding of a random tree is printed."
    parser = exec
      <$> subparser
        ( command "boltzmann" (info boltzmannParser briefDesc)
        <> command "maple" (info mapleParser briefDesc)
        <> command "uniform" (info uniformParser briefDesc) )
      <*> option (Just <$> auto)
        ( short 'n'
        <> value Nothing
        <> metavar "SAMPLE_N" )
    boltzmannParser = Boltz <$> argument (readRange 0 0.5) (metavar "PARAM[0,0.5)")
    mapleParser = Maple
      <$> argument auto (metavar "SIZE")
      <*> argument auto (metavar "PARAM[0,..)")
    uniformParser = Uniform <$> argument auto (metavar "SIZE")
    exec genType n =
      let gen = case genType of
            Boltz x -> B.gen x
            Maple s x -> resize s $ M.gen x
            Uniform s -> resize s S.gen
      in case n of
        Nothing -> generate gen >>= putStrLn . toString
        Just n -> replicateM_ n (generate gen >>= print . depth)

readRange :: (Read a, Show a, Ord a) => a -> a -> ReadM a
readRange lo hi = auto >>= \x ->
  if lo <= x && x < hi
  then return x
  else readerError $ "Parameter should be between "
    ++ show lo ++ " and " ++ show hi ++ "."

histoParser :: ParserInfo (IO ())
histoParser = info parser desc
  where
    desc = briefDesc
      <> progDesc "Show a 2D density histogram of data from stdin."
      <> header "Read one pair of decimal numbers for each line until EOF, \
        \and print a density map. Expects 4 parameters to bound the map size."
    parser = exec
      <$> argument auto (metavar "V_STEP" <> help "Vertical step size")
      <*> argument auto (metavar "V_RANGE" <> help "Vertical range")
      <*> argument auto (metavar "H_STEP" <> help "Horizontal step size")
      <*> argument auto (metavar "H_RANGE" <> help "Horizontal range")
    exec rStep rWidth cStep cWidth = do
      xs <- map read <$> lines <$> getContents :: IO [(Double, Double)]
      plotHistogram rWidth cWidth $ histogram rStep cStep xs

intToDouble :: Int -> Double
intToDouble = fromIntegral

group' [] = []
group' xs@(x : _)
  = let (ys, zs) = partition (x ==) xs
    in (x, length ys) : group' zs

-- Inner Product
ip [] _ = 0
ip _ [] = 0
ip xs'@((x, a) : xs) ys'@((y, b) : ys)
  | x < y = ip xs ys'
  | y < x = ip xs' ys
  | otherwise = a * b + ip xs ys

norm2 = sum . map (^ 2) . snd . unzip
-- Normalized Inner Product Squared
normIp2 x y = ip x y ^ 2 / norm2 x / norm2 y

