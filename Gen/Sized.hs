module Gen.Sized where

import Common.Tree

import Test.QuickCheck

-- Represents the set of trees of some fixed size as the disjoint union of
-- cartesian products.
data TreeSet = Sized { count :: Integer, union :: [(TreeSet, TreeSet)] }

treeSet :: [TreeSet]
treeSet = map withSize [0 ..]
  where
    withSize 0 = Sized 1 [] -- Leaf
    withSize n
      = let ts = take n treeSet
            union' = zip ts (reverse ts)
        in Sized (sum [count a * count b | (a, b) <- union']) union'

gen :: Gen Tree
gen = sized $ \s ->
    let set = treeSet !! s
    in index set `fmap` choose (0, count set-1) 

index (Sized 1 []) _ = Leaf
index (Sized _ ts) i
  = Node `uncurry` indexUnion i ts
  where
    indexUnion i ((a, b) : more)
      | i < count a * count b
        = let (ia, ib) = i `divMod` count b
          in (index a ia, index b ib)
      | otherwise = indexUnion (i - count a * count b) more

