Miscellaneous
=============

Code snippets from my 2015 internship.

- `Common` Common definitions.
  + `Tree.hs` Definition of the binary tree structure, depth and size.

- `Gen` Random generators.
  + `Boltzmann.hs` Boltzmann sampler of binary trees.
  + `Maple.hs` A generator found by Leo which seems to produce a
    close-to-uniform distribution for a well-chosen "amplification"
    parameter.

      One approximation of the distance to the uniform distribution
      is to compare the depth distributions.
  + `Sized.hs` Generation of trees of a given size, with uniform
    distribution.

- `Stats` Light statistic related tools.
  + `Histo2D.hs` ASCII 2D histograms.

- `Main.hs` An executable front-end of the above.

Building
========

    cabal sandbox init
    # if you care about sandboxing
    cabal install lens QuickCheck optparse-applicative
    cabal exec ghc Main -- -outputdir build
    # the -outputdir option moves the .hi/.o/... clutter aside

Running
=======

    ./Main --help
    ./Main compare --help
    ./Main compare
    ./Main compare -s 100 -r (1.3,0.01,1.4) -n 1000
    ./Main generate --help
    ./Main generate boltzmann 0.3
    ./Main generate maple 5 2
    ./Main generate uniform 5

- `godeltest/` reproduction of results from
  *Finding Test Data with Specific Properties via Metaheuristic Search*,
    R. Feldt, S. Poulding.

- `boltzmann-stats/` Experiments about projecting the sample space to
  measures (e.g., depth, size) for efficient (evalu|estim)ation.
  + `poly-expr.hs` Polynomial expressions of depth probabilities.
  + `expectation.hs` Efficient evaluation of these probabilities.
  + `fix-distri.hs` Obtain a distribution as some kind of fixed point
    (a stationary distribution of a Markov-chain-like process...?).
    Not very interesting in the case of depth (or "monotonic" functions),
    as it boils down to a less efficient equivalent of `expectation.hs`.

