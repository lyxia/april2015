{-# LANGUAGE LambdaCase, NamedFieldPuns, RecordWildCards #-}
module Stats.Histo2D where

import Control.Monad
import Data.Char
import Data.Functor
import qualified Data.IntMap as Map
import Data.IntMap (IntMap)
import Data.List
import Data.Maybe
import System.Environment
import System.Exit

-------------------
-- Printing 2D histograms and more stats in the command line
data Histo = Histo
  { histo :: IntMap (IntMap Int),
    rStep :: Double, -- rows width (row r contains [r * dStep, (r+1) * dStep])
    cStep :: Double } -- cols width

histogram :: Double -> Double -> [(Double, Double)] -> Histo
histogram rStep cStep xs
  = foldl' update ini xs
  where
    ini = Histo
      { histo = Map.empty,
        rStep, cStep }
    update m (x,y) = incr (floor $ y / rStep) (floor $ x / cStep) m
    incr y x h@Histo{..}
      = h { histo = Map.alter
              (Just . \case
                Nothing -> Map.singleton x 1
                Just m' -> Map.alter (Just . maybe 1 (+ 1)) x m') y histo }

plotHistogram :: Double -> Double -> Histo -> IO ()
plotHistogram rWidth cWidth Histo{..} = do
    forM_ [0 .. yMax] $ \r -> do
      let row = Map.findWithDefault Map.empty r histo'
      forM_ [0 .. xMax] $ \c -> do
        let n = Map.findWithDefault 0 c row
        putChar $ f n
      putChar '\n'
  where
    histo'
      = Map.mapMaybeWithKey
          (\y m -> do
            guard (0 <= y && y <= yMax)
            Just (Map.mapMaybeWithKey (\x n -> do
              guard (0 <= x && x <= xMax)
              Just n) m)) histo
    histo_sz = Map.size histo
    z = Map.foldl' (Map.foldl' (+)) 0 histo'
    yMax = floor $ rWidth / rStep
    xMax = floor $ cWidth / cStep
    f 0 = '.'
    f x = intToDigit . min 15 . max 0 $ (10 * x * histo_sz) `div` z

